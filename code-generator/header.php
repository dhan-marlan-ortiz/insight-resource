<!DOCTYPE html>
<html lang="en">
     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="description" content="">
          <meta name="author" content="">
          <link rel="icon" href="../../favicon.ico">
          <title>Code Generator | Search Page Hero Banner</title>
          <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">
          <link href="https://getbootstrap.com/docs/3.3/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
          <link href="https://getbootstrap.com/docs/3.3/examples/starter-template/starter-template.css" rel="stylesheet">
          <script src="https://getbootstrap.com/docs/3.3/assets/js/ie-emulation-modes-warning.js"></script>
     </head>
     <body>
          <nav class="navbar navbar-inverse navbar-fixed-top">
               <div class="container">
                    <div class="navbar-header">
                         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                         <span class="sr-only">Toggle navigation</span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         <span class="icon-bar"></span>
                         </button>
                         <a class="navbar-brand" href="#">Code Generator</a>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                         <ul class="nav navbar-nav">
                              <li class="active"><a href="#">Home</a></li>
                              <li><a href="#about">About</a></li>
                              <li><a href="#contact">Contact</a></li>
                         </ul>
                    </div>
               </div>
          </nav>
          <br>
          <div class="container">
               <div class="row">
                    <div class="col-sm-12">
                         <ul class="nav nav-pills">
                              <li class="active"><a href="search-page-hero-banner.php">Search Page Hero Banner</a></li>
                         </ul>
                    </div>
               </div>
          </div>
          <br>
          