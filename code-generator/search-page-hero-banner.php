<?php include("header.php"); ?>
          <div class="container">
               <div class="panel panel-primary" id="search-page">
                    <div class="panel-heading">
                         <h3 class="panel-title">Search Page Hero Banner</h3>
                    </div>
                    <div class="panel-body">
                         <form id="search-page-hero-banner-form" method="POST" action="search-page-hero-banner.php">
                              <div class="row">
                                   <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                             <label>Partner Name</label>                                             
                                             <input type="text" class="form-control" value="<?php echo (isset($_POST['partner-name']) ? $_POST['partner-name'] : ''); ?>" name="partner-name" required> 
                                        </div>
                                   </div>
                                   <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                             <label>Background URL</label>                                             
                                             <input type="text" class="form-control" value="<?php echo (isset($_POST['background-url']) ? $_POST['background-url'] : ''); ?>" name="background-url" required> 
                                        </div>
                                   </div>
                                   <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                             <label>Logo URL</label>                                             
                                             <input type="text" class="form-control" value="<?php echo (isset($_POST['logo-url']) ? $_POST['logo-url'] : ''); ?>" name="logo-url" required> 
                                        </div>
                                   </div>
                                   <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                             <label>Page Title</label>                                             
                                             <input type="text" class="form-control" value="<?php echo (isset($_POST['page-title']) ? $_POST['page-title'] : ''); ?>" name="page-title" required> 
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-sm-12">
                                        <button type="submit" name='code' class="btn btn-primary">Generate Code</button>
                                        <a href="search-page-hero-banner.php" class="btn btn-default">Reset</a>
                                   </div>
                              </div>
                         </form>
                    </div>
<?php 
if(isset($_POST['code'])) {
$template = '
<div class="hero-image-container short-hero-image" style="background-image: url('.$_POST['background-url'].'); background-position: center; background-repeat: no-repeat; padding: 80.0px 0.0px 40.0px 0.0px;">
     <div class="large-9 medium-11 small-12 text-center" style="margin: auto;">
          <img src="'.$_POST['logo-url'].'" alt="'.$_POST['partner-name'].' logo" title="'.$_POST['partner-name'].' logo">
          <h1 style="color: rgb(255,255,255);">'.$_POST['page-title'].'</h1>
          <p class="clearfix">&nbsp;</p>
     </div>
</div>';
$encoded = htmlentities($template);
echo "<div class='panel-body' id='search-page-hero-banner-code'>";
echo "<div class='form-group'><textarea class='form-control' id='generated-code' rows='8' resize='vertical'>" . $encoded . "</textarea></div>";
echo "<div class='form-group'><button class='btn btn-primary' onclick='copyToClipboard()'>Copy Text to Clipboard</button></div>";
echo "</div>";
} ?>
               </div>
          </div>

          <?php include("footer.php"); ?>
